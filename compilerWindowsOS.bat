cd ..\..
cd checktls\
pip install -r requirements.txt
pyinstaller --onefile checktls.py

DIR="C:\Python37\Scripts\checktls"

if [ -d "$DIR" ]
then
  echo "A version of checktls already exists in ${DIR}, would you like to overwrite it (y/n)?"
  read user_input
  if [ $user_input == y ]
  then
    echo "Removing old ${DIR}"
    sudo rm -rf $DIR
  else
    echo "Expecting (y/n), received '$user_input'"
  fi
fi

if [ ! -d "$DIR" ]
then
  echo "Installing files in ${DIR}..."
  sudo cp dist\checktls ${DIR}
  sudo chmod +x ${DIR}
fi

current_path=$PATH
if echo "$current_path" | grep -q $DIR
then
  echo "checktls already on PATH"
else
  echo "checktls added to PATH"
  export PATH=$PATH:C:\Python37\Scripts\checktls
fi
