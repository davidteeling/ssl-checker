#!/usr/bin/env bash

cd ../..
cd checktls/
pip install -r requirements.txt
pyinstaller --onefile checktls.py

if [[ $1 == MAC ]] && [[ $1 != LINUX ]]
then
  DIR="/usr/local/bin/checktls"

fi

if [[ $1 != MAC ]] && [[ $1 == LINUX ]]
then
  DIR="/usr/bin/checktls"
fi

if [ -d "$DIR" ]
then
  echo "A version of checktls already exists in ${DIR}, would you like to overwrite it (y/n)?"
  read user_input
  if [ $user_input == y ]
  then
    echo "Removing old ${DIR}"
    sudo rm -rf $DIR
  else
    echo "Expecting (y/n), received '$user_input'"
  fi
fi

if [ ! -d "$DIR" ]
then
  echo "Installing files in ${DIR}..."
  sudo cp dist/checktls ${DIR}
  sudo chmod +x ${DIR}
fi

current_path=$PATH
checktls_path="/usr/local/bin/checktls"
if echo "$current_path" | grep -q $DIR
then
  echo "checktls already on PATH"
else
  echo "checktls added to PATH"
  if [[ $1 == MAC ]]
  then
    export PATH=$PATH:/usr/local/bin/checktls
  else
    export PATH=$PATH:/usr/bin/checktls
  fi
fi

# clean up
rm -rf __pycache__ build dist checktls.spec
echo "Done!"
