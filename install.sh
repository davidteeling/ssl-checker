#!/bin/bash

###--- Set OS Variables ---###
LINUX="FALSE"
MAC="FALSE"

###--- Get OS ---###
OS=$(uname)

###--- Check if OS is compatible with tool ---###
case $OS in
  Linux) LINUX="TRUE";;
  Darwin) MAC="TRUE";;
  *) echo "Your OS is not compatible with this tool"
     return 1;;
esac

###--- Set Directories ---###
install_DIR=$(pwd)
command_DIR="/usr/local/bin/"
command_name="gettls"

###--- Check if tool already exists ---###
if [ -a "${command_DIR}${command_name}" ]; then
  echo "A version of gettls already exists, would you like to overwrite it (y/n)?"
  read user_input
  if [ $user_input == y ]; then
    echo "Removing old ${command_DIR}${command_name}"
    rm -f ${command_DIR}${command_name}
  else
    echo "Expecting (y/n), received '$user_input'"
  fi
fi

###--- Linux: Tool Compiler ---###
if [ $LINUX = "TRUE" ]; then

  ###--- Installing Packages ---###
  sudo yum install -y jq
  sudo yum install -y glibc-devel
  sudo yum install -y gcc
  cd /usr/src
  sudo wget http://www.datsi.fi.upm.es/~frosal/sources/shc-3.8.9.tgz
  sudo tar xzf shc-3.8.9.tgz

  ###--- Compiling SHC Source Code ---###
  cd shc-3.8.9
  sudo mkdir /usr/local/man
  sudo mkdir /usr/local/man/man1
  echo "yes" | sudo make install

  ###--- Creating Tool Binary Executable File ---###
  cd ${install_DIR}
  shc -T -f ${command_name}
  rm ${command_name}.x.c
  chmod +x ${command_name}.x
  sudo mv ${command_name}.x ${command_DIR}${command_name}

###--- MacOS: Tool Compiler ---###
elif [ $MAC = "TRUE" ]; then

  ###--- Installing Packages ---###
  brew install jq
  brew install shc
  brew install gcc

  ###--- Creating Tool Binary Executable File ---###
  echo "Creating Binary"
  cd ${install_DIR}
  shc -r -f ${command_name}
  rm ${command_name}.x.c
  chmod +x ${command_name}.x
  mv ${command_name}.x ${command_DIR}${command_name}
fi
