
const sslChecker = require("ssl-checker");
const chalk = require("chalk");

module.exports = function(slack) {

    let app = slack
    console.log('gettls plugin started')

    app.message('gettls', async ({ message, say }) => {

      // console.log(message.text)

      console.log('gettls plugin invoked');

      var n = message.text.search("http://");
      var m = message.text.search("\\|");
      var x = m - n;
      const hostname = message.text.substr(n+7,x-7);

      async function fetchdata(hostname) {
        let data = sslChecker.default(hostname)
        return data
      }

      async function main(hostname) {
        try {
          let results = await fetchdata(hostname);
          await say(`Howdy, <@${message.user}> :face_with_cowboy_hat:\n\n The SSL cert for `+hostname+` has ${results.daysRemaining} days till expiry.`);
        }
        catch (e) {
         await say(`Sorry <@${message.user}>, I couldn't check this domain for you.\n\n The error was: ` + JSON.stringify(e) + `\n\n Please ensure that you've entered an address in the format \'www.webpage.com\'`);
       }
      }

      main(hostname)

    });

    return {
    }

}
