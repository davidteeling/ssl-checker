#####################################################
#              Slackbot index.js README
#####################################################

gettls was adapted from code found at:
  https://github.com/Matty9191/ssl-cert-check


##################### myway #######################
This project is the AL [@myway](https://app.slack.com/client/T025HTK0M/D011SHQM70T/about/details) Slack bot.

Admin page: https://api.slack.com/apps/A011BK11Z2B/

##################### PURPOSE #######################

gettls provides a quick way to check a single url's ssl certificate's expiry date.

################### What is it? #####################

Its a nodejs application, powered by Slack's official Bolt framework and the official Google Cloud Firestore SDK.

[Bolt](https://slack.dev/bolt/concepts) allows us to create custom slack messages with interactivity and conversational interfaces.

The [Firestore SDK](https://googleapis.dev/nodejs/firestore/latest/) enables data persistence, allowing *@My Way* to have a memory 🧠
