## PURPOSE
gettls/checktls = tools that can be used to report on expiring SSL certificates. Their purpose is to assist with SSL/TLS auditing and management, with a goal to be used as part of a composed system that imports this data into a management/asset tracking tool, or used for monitoring.

## You have two options:

- gettls
    - written in Bash
    - supported on MacOS and LinuxOS
    - supports both YAML and JSON formatted outputs
    - take a URL/file with a list of URLs as argument

- checktls
    - written in Python
    - supported on MacOS, Linux OS and Windows OS;
    - supports both YAML and JSON formatted outputs;
    - gives you extra information about certificates (issuer, SAN, validity dates, IP address) and enables combining multiple options when running the command;
    - take a URL/file with a list of URLs as argument
    - option to output only the URLs which expires in (or less than) a number of days (option --alert)
    - note: for Windows OS machines - the code is still under testing
