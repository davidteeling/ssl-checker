## checktls ReadMe

## PURPOSE
gettls/checktls = tools that can be used to report on expiring SSL certificates. Their purpose is to assist with SSL/TLS auditing and management, with a goal to be used as part of a composed system that imports this data into a management/asset tracking tool, or used for monitoring.

- checktls
    - written in Python
    - supported on MacOS, Linux OS and Windows OS;
    - supports both YAML and JSON formatted outputs;
    - gives you extra information about certificates (issuer, SAN, validity dates, IP address) and enables combining multiple options when running the command;
    - take a URL/file with a list of URLs as argument
    - option to output only the URLs which expires in (or less than) a number of days (option --alert)
    - note: for Windows OS machines - the code is still under testing

## INSTALLATION
- Pre-requisites:
    - Python installed on the machine (versions 2.7 - 3.7)
    - git installed on the machine
- Step 1: create and activate a Python3.7 virtual environment
- Step 2: git clone this repository
- Step 3:
for Linux OS machine:
```
cd ssl-checker/
chmod +x compilerMacOS_LinuxOS.sh
./compilerMacOS_LinuxOS LINUX
```
for Mac OS machine:
```
cd ssl-checker/
chmod +x compilerMacOS_LinuxOS.sh
./compilerMacOS_LinuxOS LINUX
```
for Windows OS machine:
```
cd ssl-checker\
chmod +x compilerMacOS_LinuxOS.sh
compilerMacOS_LinuxOS LINUX
```
## Use of checktls

- Main command:
```
ARG            URL (domain name) or a text file containing a list of URLs
```
example:
```
checktls automationlogic.com
```
returns ARG, Common Name, Expiry Date and No. days until certificate expires
```
automationlogic.com automationlogic.com 2020/09/18 71
```

- Optional arguments:
```
-h, --help     show this help message and exit
-a , --alert   return brief data about the SSL/TLS certificate(s) if
               certificate expires in less then the requested number of days
-d , --date    return SSL/TLS data for a specified date; date format is
               YYYY/MM/DD
-f, --file     enable taking a text file with a list of URL(s) as an
               argument```
-i, --info     return general information about the URL's SSL/TLS
               certificate (issuer, common name, SAN, public IP address,
               validity period)```
-p , --port    port number used by the web server
-j, --json     convert query data into json format
-y, --yaml     convert query data into yaml format
```

- Example 1:
```
checktls automationlogic.com -i
```
returns general info about certificate:
```
automationlogic.com ~> ('188.121.46.11', 443)
    CommonName: automationlogic.com
    SAN: ['automationlogic.com', 'www.automationlogic.com']
    Issuer: Go Daddy Secure Certificate Authority - G2
    NotBefore: 2019-09-18 10:05:03
    NotAfter:  2020-09-18 10:05:03
```
- Example 2:
```
checktls automationlogic.com -d 2020/07/09 -j
```
returns data about the expiring date and remaining days until certificate expires formatted in a json output:
```
{"QueryString": "automationlogic.com", "CommonName": "automationlogic.com", "ExpiryDate": "2020-09-18 10:05:03", "Remaining Days": 71}
```
- Note: same feature if you add -f option to check certificates for a file
```
checktls -f path/to/file [-d 2020/07/09 -y]
```
