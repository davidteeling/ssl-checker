#!/usr/bin/env python3
import getCertificate
import dateHandling
import yamlData
from collections import namedtuple

HostInfo = namedtuple(field_names = 'cert hostname peername', typename = 'HostInfo')

def yaml_general_data(hostname, port):
    hostinfo = getCertificate.get_certificate(hostname, port)
    yamlData.yaml_general_info(hostinfo)

def yaml_query_data_today(hostname, port):
    hostinfo = getCertificate.get_certificate(hostname, port)
    yamlData.yaml_query_info(hostinfo, dateHandling.get_date_today())

def yaml_query_data(hostname, port, date):
    hostinfo = getCertificate.get_certificate(hostname, port)
    yamlData.yaml_query_info(hostinfo, date)
