#!/usr/bin/env python3

import strOutput
import dateHandling
from collections import namedtuple
import yaml

HostInfo = namedtuple(field_names = 'cert hostname peername', typename = 'HostInfo')

def yaml_general_info(hostinfo):
    hostname = hostinfo.hostname
    commonName = strOutput.get_common_name(hostinfo.cert)
    SAN = strOutput.get_alt_names(hostinfo.cert)
    issuer = strOutput.get_issuer(hostinfo.cert)
    notbefore = hostinfo.cert.not_valid_before
    notafter = hostinfo.cert.not_valid_after

    s = {
    "QueryString": hostname,
    "CommonName": commonName,
    "SAN": SAN,
    "Issuer": issuer,
    "NotBefore": str(notbefore),
    "NotAfter": str(notafter)
    }
    print(yaml.dump(s))

def yaml_query_info(hostinfo, date):
    hostname = hostinfo.hostname
    commonname = strOutput.get_common_name(hostinfo.cert)
    notafter = hostinfo.cert.not_valid_after
    remainingdays = dateHandling.get_remaining_days(date, dateHandling.convert_datetime_to_date(str(hostinfo.cert.not_valid_after)))

    s = {
    "QueryString": hostname,
    "CommonName": commonname,
    "ExpiryDate": str(notafter),
    "Remaining Days": remainingdays
    }
    print(yaml.dump(s))
