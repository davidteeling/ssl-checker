#!/usr/bin/env python3
import strOutput
import dateHandling
from collections import namedtuple

HostInfo = namedtuple(field_names = 'cert hostname peername', typename = 'HostInfo')

def print_general_info(hostinfo):
    s = '''{hostname} ~> {peername}
    CommonName: {commonname}
    SAN: {SAN}
    Issuer: {issuer}
    NotBefore: {notbefore}
    NotAfter:  {notafter}
    '''.format(
            hostname = hostinfo.hostname,
            peername = hostinfo.peername,
            commonname = strOutput.get_common_name(hostinfo.cert),
            SAN = strOutput.get_alt_names(hostinfo.cert),
            issuer = strOutput.get_issuer(hostinfo.cert),
            notbefore = hostinfo.cert.not_valid_before,
            notafter = hostinfo.cert.not_valid_after
    )
    print(s)
    #Reference for script to get certificate: Дамјан Георгиевски, <script src="https://gist.github.com/gdamjan/55a8b9eec6cf7b771f92021d93b87b2c.js"></script>

def print_query_info(hostinfo, date):
    s = '''
    QueryString: {hostname}
    CommonName: {commonname}
    ExpiryDate:  {notafter}
    Remaining Days: {remainingdays}
    '''.format(
            hostname = hostinfo.hostname,
            commonname = strOutput.get_common_name(hostinfo.cert),
            notafter = hostinfo.cert.not_valid_after,
            remainingdays = dateHandling.get_remaining_days(date, dateHandling.convert_datetime_to_date(str(hostinfo.cert.not_valid_after)))
    )
    print(s)

def print_expiry_date(hostinfo, date):
    s = '''{hostname} {commonname} {notafter} {remainingdays}'''.format(
        hostname = hostinfo.hostname,
        commonname = strOutput.get_common_name(hostinfo.cert),
        notafter = dateHandling.convert_datetime_to_date(str(hostinfo.cert.not_valid_after)),
        remainingdays = dateHandling.get_remaining_days(date, dateHandling.convert_datetime_to_date(str(hostinfo.cert.not_valid_after)))
    )
    print(s)

def print_set_alert(hostinfo, date, days):
    hostname = hostinfo.hostname
    commonname = strOutput.get_common_name(hostinfo.cert)
    notafter = dateHandling.convert_datetime_to_date(str(hostinfo.cert.not_valid_after))
    remainingdays = dateHandling.get_remaining_days(date, dateHandling.convert_datetime_to_date(str(hostinfo.cert.not_valid_after)))
    if remainingdays <= days:
        print(hostname, " ", commonname, " ", str(notafter), " ", str(remainingdays))
    else:
        pass
