#!/usr/bin/env python3

# Reference for script to get certificate data: Дамјан Георгиевски, <script src="https://gist.github.com/gdamjan/55a8b9eec6cf7b771f92021d93b87b2c.js"></script>
from OpenSSL import SSL
from cryptography import x509
from cryptography.x509.oid import NameOID
from socket import socket

def get_alt_names(cert):
    try:
        ext = cert.extensions.get_extension_for_class(x509.SubjectAlternativeName)
        return ext.value.get_values_for_type(x509.DNSName)
    except x509.ExtensionNotFound:
        return None

def get_common_name(cert):
    try:
        names = cert.subject.get_attributes_for_oid(NameOID.COMMON_NAME)
        return names[0].value
    except x509.ExtensionNotFound:
        return None

def get_issuer(cert):
    try:
        names = cert.issuer.get_attributes_for_oid(NameOID.COMMON_NAME)
        return names[0].value
    except x509.ExtensionNotFound:
        return None
