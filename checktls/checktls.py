#!/usr/bin/env python3
import argparse
import checkURL
import getURLsFile
import getCertificate
import strOutput
import printCertData
import getCertData
import dateHandling
import slackAlert
import getYamlData
import getJsonData
import sys

parser = argparse.ArgumentParser(description = 'It is a tool for getting information about SSL/TLS certificates for any URL. Usage can be modified with various options. Takes an URL or a text file (with a list of URLs) as an argument and returns the expiry date for the website\'s tls/ssl certificate and the remaining days until expiration', prog='checktls')

date_dependency = parser.add_mutually_exclusive_group()
format = parser.add_mutually_exclusive_group()

parser.add_argument('ARG', help = 'URL (domain name) or a text file containing a list of URLs', type = str)
format.add_argument('-a', '--alert', metavar='', help='return brief data about the SSL/TLS certificate(s) if certificate expires in less then the requested number of days ', type=int, default=None)
date_dependency.add_argument('-d', '--date', metavar='', help='return SSL/TLS data for a specified date; date format is YYYY/MM/DD', type=str, default=None)
parser.add_argument('-f', '--file', action='store_true', help='enable taking a text file with a list of URL(s) as an argument')
date_dependency.add_argument('-i', '--info', action='store_true', help='return general information about the URL\'s SSL/TLS certificate (issuer, common name, SAN, public IP address, validity period)')
parser.add_argument('-p', '--port', metavar='', action='store', default=443, help='port number used by the web server', type=int)
format.add_argument('-j', '--json', action='store_true', help='convert query data into json format')
format.add_argument('-y', '--yaml', action='store_true', help='convert query data into yaml format')
args = parser.parse_args()

# Take Argument as an URL
if not args.file and args.date is None and not args.json and not args.yaml and not args.info and args.alert is None:
    try:
        checkURL.check_url(args.ARG)
        getCertData.get_expiry_date_today(args.ARG, args.port)
    except:
        print("ERROR: The URL doesn't exist. The argument needs to be a valid URL.")
        sys.exit(1)
elif not args.file and args.date is not None and not args.json and not args.yaml and not args.info and args.alert is None:
    try:
        checkURL.check_url(args.ARG)
    except:
        print("ERROR: The URL doesn't exist. The argument needs to be a valid URL.")
        sys.exit(1)
    try:
        getCertData.get_expiry_date(args.ARG, args.port, args.date)
    except:
        print("ERROR: Change the date format. A valid date must be written as YYYY/MM/DD.")
        sys.exit(1)
elif not args.file and args.date is not None and args.json and not args.yaml and not args.info and args.alert is None:
    try:
        checkURL.check_url(args.ARG)
    except:
        print("ERROR: The URL doesn't exist. The argument needs to be a valid URL.")
        sys.exit(1)
    try:
        getJsonData.json_query_data(args.ARG, args.port, args.date)
    except:
        print("ERROR: Change the date format. A valid date must be written as YYYY/MM/DD.")
        sys.exit(1)
elif not args.file and args.date is not None and not args.json and args.yaml and not args.info and args.alert is None:
    try:
        checkURL.check_url(args.ARG)
    except:
        print("ERROR: The URL doesn't exist. The argument needs to be a valid URL.")
        sys.exit(1)
    try:
        getYamlData.yaml_query_data(args.ARG, args.port, args.date)
    except:
        print("ERROR: Change the date format. A valid date must be written as YYYY/MM/DD.")
        sys.exit(1)
elif not args.file and args.date is None and args.json and not args.yaml and not args.info and args.alert is None:
    try:
        checkURL.check_url(args.ARG)
        getJsonData.json_query_data_today(args.ARG, args.port)
    except:
        print("ERROR: The URL doesn't exist. The argument needs to be a valid URL.")
        sys.exit(1)
elif not args.file and args.date is None and not args.json and args.yaml and not args.info and args.alert is None:
    try:
        checkURL.check_url(args.ARG)
        getYamlData.yaml_query_data_today(args.ARG, args.port)
    except:
        print("ERROR: The URL doesn't exist. The argument needs to be a valid URL.")
        sys.exit(1)
elif not args.file and args.date is None and not args.json and not args.yaml and args.info and args.alert is None:
    try:
        checkURL.check_url(args.ARG)
        getCertData.get_general_data(args.ARG, args.port)
    except:
        print("ERROR: The URL doesn't exist. The argument needs to be a valid URL.")
        sys.exit(1)
elif not args.file and args.json and not args.yaml and args.info and args.alert is None:
    try:
        checkURL.check_url(args.ARG)
        getJsonData.json_general_data(args.ARG, args.port)
    except:
        print("ERROR: The URL doesn't exist. The argument needs to be a valid URL.")
        sys.exit(1)
elif not args.file and not args.json and args.yaml and args.info and args.alert is None:
    try:
        checkURL.check_url(args.ARG)
        getYamlData.yaml_general_data(args.ARG, args.port)
    except:
        print("ERROR: The URL doesn't exist. The argument needs to be a valid URL.")
        sys.exit(1)
elif not args.file and args.date is None and not args.json and not args.yaml and not args.info and args.alert is not None:
    try:
        checkURL.check_url(args.ARG)
        getCertData.get_alert_today(args.ARG, args.port, args.alert)
    except:
        print("ERROR: The URL doesn't exist. The argument needs to be a valid URL.")
        sys.exit(1)
elif not args.file and args.date is not None and not args.json and not args.yaml and not args.info and args.alert is not None:
    try:
        checkURL.check_url(args.ARG)
    except:
        print("ERROR: The URL doesn't exist. The argument needs to be a valid URL.")
        sys.exit(1)
    try:
        getCertData.get_alert(args.ARG, args.port, args.date, args.alert)
    except:
        print("ERROR: Change the date format. A valid date must be written as YYYY/MM/DD.")
        sys.exit(1)

##------ Take Argument as File with List of URLs-------##
elif args.file and args.date is None and not args.json and not args.yaml and not args.info and args.alert is None:
    try:
        for host in getURLsFile.get_urls_file(args.ARG):
            checkURL.check_url(host)
            getCertData.get_expiry_date_today(host, args.port)
    except:
        print("ERROR: No such file or directory. The argument needs to be a valid file.")
        sys.exit(1)
elif args.file and args.date is not None and not args.json and not args.yaml and not args.info and args.alert is None:
    for host in getURLsFile.get_urls_file(args.ARG):
        try:
            checkURL.check_url(host)
        except:
            print("ERROR: No such file or directory. The argument needs to be a valid file.")
            sys.exit(1)
        try:
            getCertData.get_expiry_date(host, args.port, args.date)
        except:
            print("ERROR: Change the date format. A valid date must be written as YYYY/MM/DD.")
            sys.exit(1)
elif args.file and args.date is not None and args.json and not args.yaml and not args.info and args.alert is None:
    for host in getURLsFile.get_urls_file(args.ARG):
        try:
            checkURL.check_url(host)
        except:
            print("ERROR: No such file or directory. The argument needs to be a valid file.")
            sys.exit(1)
        try:
            getJsonData.json_query_data(host, args.port, args.date)
        except:
            print("ERROR: Change the date format. A valid date must be written as YYYY/MM/DD.")
            sys.exit(1)
elif args.file and args.date is not None and not args.json and args.yaml and not args.info and args.alert is None:
    for host in getURLsFile.get_urls_file(args.ARG):
        try:
            checkURL.check_url(host)
        except:
            print("ERROR: No such file or directory. The argument needs to be a valid file.")
            sys.exit(1)
        try:
            getYamlData.yaml_query_data(host, args.port, args.date)
        except:
            print("ERROR: Change the date format. A valid date must be written as YYYY/MM/DD.")
            sys.exit(1)
elif args.file and args.date is None and args.json and not args.yaml and not args.info and args.alert is None:
    try:
        for host in getURLsFile.get_urls_file(args.ARG):
            checkURL.check_url(host)
            getJsonData.json_query_data_today(host, args.port)
    except:
        print("ERROR: No such file or directory. The argument needs to be a valid file.")
        sys.exit(1)
elif args.file and args.date is None and not args.json and args.yaml and not args.info and args.alert is None:
    try:
        for host in getURLsFile.get_urls_file(args.ARG):
            checkURL.check_url(host)
            getYamlData.yaml_query_data_today(host, args.port)
    except:
        print("ERROR: No such file or directory. The argument needs to be a valid file.")
        sys.exit(1)
elif args.file and args.date is None and not args.json and not args.yaml and args.info and args.alert is None:
    try:
        for host in getURLsFile.get_urls_file(args.ARG):
            checkURL.check_url(host)
            getCertData.get_general_data(host, args.port)
    except:
        print("ERROR: No such file or directory. The argument needs to be a valid file.")
        sys.exit(1)
elif args.file and args.json and not args.yaml and args.info and args.alert is None:
    try:
        for host in getURLsFile.get_urls_file(args.ARG):
            checkURL.check_url(host)
            getJsonData.json_general_data(host, args.port)
    except:
        print("ERROR: No such file or directory. The argument needs to be a valid file.")
        sys.exit(1)
elif args.file and not args.json and args.yaml and args.info and args.alert is None:
    try:
        for host in getURLsFile.get_urls_file(args.ARG):
            checkURL.check_url(host)
            getYamlData.yaml_general_data(host, args.port)
    except:
        print("ERROR: No such file or directory. The argument needs to be a valid file.")
        sys.exit(1)
elif args.file and args.date is None and not args.json and not args.yaml and not args.info and args.alert is not None:
    try:
        for host in getURLsFile.get_urls_file(args.ARG):
            checkURL.check_url(host)
            getCertData.get_alert_today(host, args.port, args.alert)
    except:
        print("ERROR: No such file or directory. The argument needs to be a valid file.")
        sys.exit(1)
elif args.file and args.date is not None and not args.json and not args.yaml and not args.info and args.alert is not None:
    for host in getURLsFile.get_urls_file(args.ARG):
        try:
            checkURL.check_url(host)
        except:
            print("ERROR: No such file or directory. The argument needs to be a valid file.")
            sys.exit(1)
        try:
            getCertData.get_alert(host, args.port, args.date, args.alert)
        except:
            print("ERROR: Change the date format. A valid date must be written as YYYY/MM/DD.")
            sys.exit(1)
else:
    pass
