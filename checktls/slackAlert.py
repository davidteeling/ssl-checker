#!/usr/bin/env python3
import json
import requests

def alert_msg(urlHost, days):
    slackHook = "https://hooks.slack.com/services/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"
    slackChannel = "#academy_api_testing"
    slackUsername = "SSL-checker"
    slackMessage = "Remainder! TLS/SSL certificate(s) for " + urlHost + " expires in " + str(days)
    obj = json.loads('{"channel": slackChannel, "username": slackUsername, "text": slackMessage}')
    r = requests.post(URL_slack, json=obj)
