#!/usr/bin/env python3
from datetime import date
from datetime import datetime

def get_date_today():
    date = datetime.today().strftime('%Y/%m/%d')
    return date

def get_remaining_days(date, expiry_date):
    delta = datetime.strptime((expiry_date), '%Y/%m/%d') - datetime.strptime(date, '%Y/%m/%d')
    return delta.days

def convert_datetime_to_date(date):
    return date[:4] + "/" + date[5:7] + "/" + date[8:10]
