#!/usr/bin/env python3
import getCertificate
import printCertData
import dateHandling
from collections import namedtuple

HostInfo = namedtuple(field_names = 'cert hostname peername', typename = 'HostInfo')

def get_general_data(hostname, port):
    hostinfo = getCertificate.get_certificate(hostname, port)
    printCertData.print_general_info(hostinfo)

def get_query_data_today(hostname, port, date):
    hostinfo = getCertificate.get_certificate(hostname, port)
    printCertData.print_query_info(hostinfo, dateHandling.get_date_today())

def get_query_data(hostname, port, date):
    hostinfo = getCertificate.get_certificate(hostname, port)
    printCertData.print_query_info(hostinfo, date)

def get_expiry_date_today(hostname, port):
    hostinfo = getCertificate.get_certificate(hostname, port)
    printCertData.print_expiry_date(hostinfo, dateHandling.get_date_today())

def get_expiry_date(hostname, port, date):
    hostinfo = getCertificate.get_certificate(hostname, port)
    printCertData.print_expiry_date(hostinfo, date)

def get_alert(hostname, port, date, days):
    hostinfo = getCertificate.get_certificate(hostname, port)
    printCertData.print_set_alert(hostinfo, date, days)

def get_alert_today(hostname, port, days):
    hostinfo = getCertificate.get_certificate(hostname, port)
    printCertData.print_set_alert(hostinfo, dateHandling.get_date_today(), days)
