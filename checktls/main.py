#!/usr/bin/env python3
import sys
import checkURL
import getURLsFile
import getCertificate
import strOutput
import printCertData
import getCertData
import dateHandling
import slackAlert
import getYamlData
import getJsonData

HOST = sys.argv[1]
PORT = sys.argv[2]
date = sys.argv[3]

# Check url (check the input)
checkURL.check_url(HOST)

# Get certificate data
print(" ")
getCertData.get_general_data(HOST, int(PORT))

# Get query Data
getCertData.get_query_data_today(HOST, int(PORT), date)
getCertData.get_query_data(HOST, int(PORT), date)

# Get expiry Date
getCertData.get_expiry_date_today(HOST, int(PORT))
getCertData.get_expiry_date(HOST, int(PORT), date)

# Store Data yaml
getYamlData.yaml_general_data(HOST, int(PORT))
getYamlData.yaml_query_data_today(HOST, int(PORT))
getYamlData.yaml_query_data(HOST, int(PORT), date)

# Store Data Json
getJsonData.json_general_data(HOST, int(PORT))
getJsonData.json_query_data_today(HOST, int(PORT))
getJsonData.json_query_data(HOST, int(PORT), date)
# Alert on Slack
getCertData.get_alert(HOST, int(PORT), date, 100)
getCertData.get_alert(HOST, int(PORT), date, 100)
