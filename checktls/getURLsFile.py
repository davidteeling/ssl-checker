#!/usr/bin/env python3

def get_urls_file(filename):
    urls = []
    file = open(filename, "r")
    if file.mode == 'r':
        content = file.readlines()
    for x in content:
        urls.append(x.rstrip("\n"))
    return urls
