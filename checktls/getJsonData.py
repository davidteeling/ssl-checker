#!/usr/bin/env python3
import getCertificate
import dateHandling
import jsonData
from collections import namedtuple

HostInfo = namedtuple(field_names = 'cert hostname peername', typename = 'HostInfo')

def json_general_data(hostname, port):
    hostinfo = getCertificate.get_certificate(hostname, port)
    jsonData.json_general_info(hostinfo)

def json_query_data_today(hostname, port):
    hostinfo = getCertificate.get_certificate(hostname, port)
    jsonData.json_query_info(hostinfo, dateHandling.get_date_today())

def json_query_data(hostname, port, date):
    hostinfo = getCertificate.get_certificate(hostname, port)
    jsonData.json_query_info(hostinfo, date)
