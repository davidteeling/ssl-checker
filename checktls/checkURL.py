#!/usr/bin/env python3
import sys
import requests

def check_url(url):
    if not url.startswith("https://"):
        url = "https://" + url
    try:
        r = requests.get(url)
        if r.status_code != 200:
            print("Request error, please check the URL exists", url, r.status_code)
            sys.exit(1)
    except:
        print("Connection to the URL refused. Please check the URL again")
