#####################################################
#                   gettls ReadMe
#####################################################

gettls was adapted from code found at:
  https://github.com/Matty9191/ssl-cert-check
  Author: < matty91 at gmail dot com >

##################### PURPOSE #######################

gettls a tool that can be used to report on expiring SSL certificates. It's purpose is to assist with SSL/TLS auditing and management, with a goal to be used as part of a composed system that imports this data into a management/asset tracking tool, or used for monitoring.

It supports both **YAML** and **JSON** formatted outputs.

################## INSTALLATION #####################

Run the following commands:

1) chmod +x install.sh
2) ./install.sh
 
The user needs these commands:

- awk
- date
- grep
- openssl
- printf
- sed
- mktemp
- find
- jq

The user is prompted to install any packages that aren't found.

##################### USAGE #########################

Usage:
    % gettls <option> <argument>

#       -b                : Will not print header
        -f cert file      : File with a list of FQDNs and ports
#       -h                : Print this screen
        -j                : Output in JSON format
#       -p port           : Port to connect to (interactive mode)
        -q                : Don't print anything on the console
#       -s commmon name   : Server to connect to (interactive mode)
        -V                : Print version information
#       -x days           : Certificate expiration interval (eg. if cert_date < days)
        -y                : Output in YAML format

*E.G
  % gettls -s www.google.com -j

Produces:
  [
  {
    "QueryString": "www.google.com",
    "QueryDate": "2020/07/06",
    "ExpiryDate": "2020/09/09",
    "DaysRemaining": "65"
  }
  ]*
